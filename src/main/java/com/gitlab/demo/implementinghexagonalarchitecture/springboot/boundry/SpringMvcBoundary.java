package com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry;

import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driven_port.IObtainPoems;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driver_port.IReactToCommands;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.driver_adapter.SpringMvcPublisher;
import org.springframework.ui.Model;

public class SpringMvcBoundary {
    private final IObtainPoems poemObtainer;

    public SpringMvcBoundary(IObtainPoems poemObtainer) {
        this.poemObtainer = poemObtainer;
    }

    public IReactToCommands basedOn(Model webModel) {
        SpringMvcPublisher webPublisher = new SpringMvcPublisher(webModel);
        IReactToCommands boundary = new Boundary(poemObtainer, webPublisher);
        return boundary;
    }
}
