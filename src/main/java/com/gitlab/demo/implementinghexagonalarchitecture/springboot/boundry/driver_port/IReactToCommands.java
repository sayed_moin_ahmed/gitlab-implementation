package com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driver_port;

/**
 * https://www.freecodecamp.org/news/implementing-a-hexagonal-architecture/
 */
public interface IReactToCommands {
    void reactTo(Object command);
}
