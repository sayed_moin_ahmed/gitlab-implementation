package com.gitlab.demo.implementinghexagonalarchitecture.springboot.driven_adapter;

import com.gitlab.demo.implementinghexagonalarchitecture.springboot.PoemRepository;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driven_port.IObtainPoems;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.internal.domain.Poem;

import java.util.Collection;
import java.util.stream.Collectors;

public class PoemRepositoryAdapter implements IObtainPoems {
    private PoemRepository poemRepository;

    public PoemRepositoryAdapter(PoemRepository poemRepository) {
        this.poemRepository = poemRepository;
    }

    @Override
    public String[] getMePoems(String language) {
        Collection<Poem> poems = poemRepository.findByLanguage(language);
        final String[] poemsArray = poems.stream()
                .map(p -> p.getText())
                .collect(Collectors.toList())
                .toArray(new String[0]);
        return poemsArray;
    }
}