package com.gitlab.demo.implementinghexagonalarchitecture.springboot;

import java.util.Collection;

import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.internal.domain.Poem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PoemRepository extends CrudRepository<Poem, Long> {
    Collection<Poem> findByLanguage(String language);
}