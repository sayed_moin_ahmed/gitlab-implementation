package com.gitlab.demo.implementinghexagonalarchitecture.springboot.driven_adapter;

import java.util.Objects;

import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driven_port.IWriteLines;
import org.springframework.ui.Model;


public class SpringMvcPublisher implements IWriteLines {
    static final String LINES_ATTRIBUTE = "lines";

    private Model webModel;

    public SpringMvcPublisher(Model webModel) {
        this.webModel = webModel;
    }

    public void writeLines(String lines) {
        Objects.requireNonNull(lines);
        webModel.addAttribute(LINES_ATTRIBUTE, lines);
    }
}